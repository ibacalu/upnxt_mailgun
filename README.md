# UpnxtMailgun

MailGun API Command Line

## Installation

    $ gem install upnxt_mailgun

## Usage

```
Usage: mg [options]

Available options:
    -d, --domain DOMAIN              Domain Filter. Supports multiple input.
    -u, --user EMAIL                 User Email (ex. john.doe@unifiedpost.com)
    -k, --key MAILGUN_API_KEY        Provide Mailgun API Key
    -a, --action ACTION              Specify action [list, block, unblock]
    -l, --limit LIMIT                Display limit (max 10.000, default: 100)

Help options:
    -v, --version                    show version information
    -h, --help                       Show this message
```

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/ibacalu/upnxt_mailgun.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

