# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'upnxt_mailgun/version'

Gem::Specification.new do |gem|
  gem.name          = "upnxt_mailgun"
  gem.version       = UpnxtMailgun::VERSION
  gem.authors       = ["Iulian Bacalu"]
  gem.email         = ["iulian.bacalu@up-nxt.com"]

  gem.summary       = %q{Manage Bounces with the Mailgun API}
  gem.description   = %q{List/Add/Remove Bounces with the Mailgun API}
  gem.homepage      = "http://up-nxt.com"
  gem.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if gem.respond_to?(:metadata)
    gem.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  gem.files         = Dir.glob('lib/**/*') + Dir.glob('bin/**/*') +
                      %w(bin/mg)
  gem.bindir        = "bin"
  gem.executables   = %w(mg)
  gem.require_paths = ["lib"]

  gem.add_development_dependency "bundler", "~> 1.12"
  gem.add_development_dependency "rake", "~> 10.0"
  gem.add_development_dependency "rspec", "~> 3.0"
end
