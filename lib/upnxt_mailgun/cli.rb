require 'optparse'
require 'ostruct'

module UpnxtMailgun

  module CLI
    class MissingArgumentException < Exception;
    end

    def self.parse(args)
      options = ::OpenStruct.new
      options.domains = []
      options.users = []
      options.key = nil
      options.action = nil
      options.limit = 100

      opts = OptionParser.new do |o|
        o.banner = "Usage: mg [options]"

        o.separator ''
        o.separator 'Available options:'
        o.on('-d', '--domain DOMAIN', "Domain Filter. Supports multiple input.") do |d|
          options.domains << d
        end
        o.on('-u', '--user EMAIL', "User Email (ex. john.doe@unifiedpost.com)") do |u|
          options.users << u
        end
        o.on('-k', '--key MAILGUN_API_KEY', "Provide Mailgun API Key") do |key|
          options.key = key
        end
        o.on('-a', '--action ACTION', [:list, :block, :unblock], "Specify action [list, block, unblock]") do |a|
          options.action = a
        end
        o.on('-l', '--limit LIMIT', "Display limit (max 10.000, default: 100)") do |l|
          options.limit = l
        end

        o.separator ''
        o.separator 'Help options:'
        o.on('-v', '--version', 'show version information') do
          puts UpnxtMailgun::VERSION
          exit
        end
        o.on_tail('-h', '--help', 'Show this message') do
          puts o
          exit
        end
      end

      opts.parse!(args)

      if options.action.nil?
        options.action = :list
      end

      raise UpnxtMailgun::CLI::MissingArgumentException, 'ERROR: Please provide at least 1 domain!' if options.domains.empty?

      if options.key.nil?
        raise UpnxtMailgun::CLI::MissingArgumentException, "ERROR: Please specify Mailgun API Key!" if ENV['MAILGUN_API_KEY'].nil?
        options.key = ENV['MAILGUN_API_KEY']
      end

      options.args = args
      options
    end
  end
end