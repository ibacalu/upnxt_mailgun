#!/usr/bin/env ruby

require 'mailgun'
require 'yaml'
require 'optparse'
require 'ostruct'
require "upnxt_mailgun/version"
require "upnxt_mailgun/cli"


module UpnxtMailgun
  def self.run!(cli)
    if cli.action == :list
      cli.domains.each do |domain|
        UpnxtMailgun::display_bounces(cli.key, domain, cli.limit)
      end

    elsif cli.action == :block
      raise UpnxtMailgun::CLI::MissingArgumentException, 'Error: Please provide at least 1 user!' if cli.users.empty?

      cli.domains.each do |domain|
        cli.users.each do |user|
          puts "Adding bounce for '#{user}' on '#{domain}'..."
          bounce = UpnxtMailgun::add_bounce(cli.key, domain, user)
          puts bounce
        end
      end

    elsif cli.action == :unblock
      raise UpnxtMailgun::CLI::MissingArgumentException, 'Error: Please provide at least 1 user!' if cli.users.empty?

      cli.domains.each do |domain|
        cli.users.each do |user|
          puts "Removing bounce for '#{user}' on '#{domain}'..."
          bounce = UpnxtMailgun::del_bounce(cli.key, domain, user)
          puts bounce
        end
      end
    end
  end

  def self.display_bounces(_api_key, _domain, _limit)
    # puts "API: #{_api_key}"
    puts "+ Domain: #{_domain}"
    puts "+ Limit: #{_limit}"
    mailgun_api = Mailgun::Client.new "#{_api_key}"
    bounces = mailgun_api.get("#{_domain}/bounces", {:limit => "#{_limit}"}).to_h
    bounces.each do |i,v|
      if i == 'items'
        index = 0
        v.each do |item|
          index += 1
          puts "#{index}. #{item["address"]}"
        end
      end
    end
    print '+' * 30
    puts
  end

  def self.add_bounce(_api_key, _domain, _user)
    mailgun_api = Mailgun::Client.new "#{_api_key}"
    _result = mailgun_api.post("#{_domain}/bounces", {:address => "#{_user}"}).to_h
    return _result
  end

  def self.del_bounce(_api_key, _domain, _user)
    mailgun_api = Mailgun::Client.new "#{_api_key}"
    _result = mailgun_api.delete("#{_domain}/bounces/#{_user}").to_h
    return _result
  end
end